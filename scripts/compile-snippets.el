#!/usr/bin/env -S emacs --script



;; Copyright 2023 Gentoo Authors


;; This file is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.



;;; Code:


(defvar snippets-directory
  (expand-file-name "../snippets" (file-name-directory load-file-name)))


(message "Snippets directory: %s" snippets-directory)

(if (equal (require 'yasnippet nil t) 'yasnippet)
    (yas-compile-directory snippets-directory)
  (display-warning 'compile-snippets "yasnippet could not be loaded" :warning))



;;; compile-snippets.el ends here
